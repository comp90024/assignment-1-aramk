#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

PBS_RUN="./pbs/pbs_run.sh"
PBS_MERGE="./pbs/pbs_merge.sh"
# PBS_LOG="./pbs/pbs_log.sh"
# RUNNER="./src/run.py"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

if [ $# -lt 4 ]; then
	echo '$ ./run_parallel.sh <user> <number of nodes> <source_file> <target_file>'
	exit 1
fi
USER=$1
NODE_COUNT=$2
SOURCE_FILE=$3
TARGET_FILE=$4

# Unique ID for this run.
# TASK_ID="words-$(uuidgen)"
TASK_ID="words-$(basename $SOURCE_FILE)_"$NODE_COUNT"n"
echo "Running Parallel Task # $TASK_ID for user '$USER' with $NODE_COUNT nodes, $(date "+%Y-%m-%d %H:%M:%S")"
mkdir $TASK_ID
rm -rf $TASK_ID/*
echo "Using Directory: $(pwd)/$TASK_ID"

# Split the source file into pieces
# SOURCE_PIECES=$(python ./util/split_file.py $SOURCE_FILE $NODE_COUNT -l 1 -o $TASK_ID)
SPLIT_FILES_STR=$(python ./util/split_file.py -l 1 -o $TASK_ID $SOURCE_FILE $NODE_COUNT)
# IFS=' ' read -a SPLIT_FILES <<< "$SPLIT_FILES_STR"
echo "Source File Pieces: $SPLIT_FILES_STR"
SOURCE_FILE_PREFIX="$TASK_ID/$(basename $SOURCE_FILE)"
echo "Source File Prefix: $SOURCE_FILE_PREFIX"

# TODO do we set nodes here?
# Runs the PBS script once for each number of nodes. This is treated as a single job.
NAME=$SOURCE_FILE"_"$NODE_COUNT"n"
# TODO can't seem to reuse $JOB_ARGS due to double quotes
# JOB_ARGS="-v \"SOURCE_FILE=$SOURCE_FILE, TARGET_FILE=$TARGET_FILE, TASK_ID=$TASK_ID\" -o $TASK_ID -e $TASK_ID"
JOB_ID=$(qsub $PBS_RUN -N $NAME -l nodes=$NODE_COUNT:ppn=1 -v "SOURCE_FILE=$SOURCE_FILE, SOURCE_FILE_PREFIX=$SOURCE_FILE_PREFIX, TARGET_FILE=$TARGET_FILE, TASK_ID=$TASK_ID" -o $TASK_ID -e $TASK_ID -j oe -t 1-$NODE_COUNT)
echo "Job $JOB_ID Started"
MERGE_JOB_ID=$(qsub $PBS_MERGE -N $NAME"_merge" -l nodes=2:ppn=1 -W depend=afterokarray:$JOB_ID -v "SOURCE_FILE=$SOURCE_FILE, SOURCE_FILE_PREFIX=$SOURCE_FILE_PREFIX, TARGET_FILE=$TARGET_FILE, TASK_ID=$TASK_ID" -o $TASK_ID -e $TASK_ID -j oe)
echo "Merge Job $MERGE_JOB_ID Started"
