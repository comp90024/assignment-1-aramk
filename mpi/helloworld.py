from mpi4py import MPI
import sys

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
size = comm.Get_size()
name = MPI.Get_processor_name()
MASTER = 0

if rank == MASTER:
    for i in range(1, size):
        greeting = "Hello World!"
        comm.send(greeting, dest=i, tag=1)
        comm.send(i, dest=i, tag=2)
        print 'sent %s to dest %d' % (greeting, i)
else:
    input1 = comm.recv(source=MASTER, tag=1)
    input2 = comm.recv(source=MASTER, tag=2)
    print 'received %s and %s from master' % (input1, input2)
    # sys.stdout.write("%s from %d on %s. Received data:%d\n" %(input1, rank, name, input2))
