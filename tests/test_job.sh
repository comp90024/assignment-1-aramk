#!/bin/bash

USER='aramk'
PBS_FILE='./test_pbs.sh'
# Stop and delete current jobs
qselect -u $USER | xargs -L1 qdel
# Start the PBS job
JOB_ID=$(qsub $PBS_FILE)
# Print stats
qstat -u $USER
sleep 2s
checkjob $JOB_ID
