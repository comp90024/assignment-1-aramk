#!/bin/bash

#PBS -N Assignment_1
#PBS -A pMelb0164
#PBS -q serial
#PBS -l nodes=1:ppn=1
#PBS -l walltime=01:00:00
#PBS -l pmem=2000mb

pwd
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

module load python/2.7.3-gcc
pwd
python ./test.py
