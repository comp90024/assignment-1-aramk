#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Runs a PBS script as a new job.

# Read args
if [ $# -lt 2 ]; then
	echo '$ ./run_job.sh <user> <pbs_file>'
	exit 1
fi
if [ ! -z "$1" ]; then
	USER=$1
fi
if [ ! -z "$2" ]; then
	PBS_FILE=$2
fi

echo "Running Job from $(pwd)"

# Start the PBS job
JOB_ID=$(qsub $PBS_FILE)
echo "Job ID String $JOB_ID"
# Get the job ID number
JOB_ID_NUMBER=$(echo $JOB_ID | sed -e 's/[^\0-9]*$//')
echo "Job ID Number $JOB_ID_NUMBER"
# Print stats
qstat -u $USER
sleep 2s
checkjob $JOB_ID_NUMBER
