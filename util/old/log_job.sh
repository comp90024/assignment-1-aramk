#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Logs information about a job.

if [ $# -lt 1 ]; then
	echo '$ ./log_job.sh <job_id>'
	exit 1
fi
JOB_IDS=$1

function log_func {
    echo "$(echo "$1" | grep "$2")"
}

for JOB_ID in $JOB_IDS
do
	if [[ $JOB_ID == *"[]"* ]]
	then
		echo "Logging Array Job $JOB_ID"
		LOG=$(qstat -f $JOB_ID -t)
	else
		echo "Logging Job $JOB_ID"
		LOG=$(qstat -f $JOB_ID)
	fi
	log_func $LOG "wall"
done
