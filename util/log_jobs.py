#!/usr/bin/env python

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Logs information about all jobs in the user's queue.
# This needs Python 2.7+

import sys, re, os, subprocess

if len(sys.argv) < 2:
	print '$ python ./log_jobs.py <user>'
	exit()
else:
	user = sys.argv[1]

try:
	cmd = 'qstat -u ' + user
	result = subprocess.check_output(cmd, shell=True)
except Exception as e:
	print 'Error: %s' % e
	exit()

if len(result) == 0:
	print 'No jobs exist'
	exit()

jobs = re.findall(r'^\s*\d+\S+', result, flags=re.M)
if len(jobs):
	for job_id in jobs:
		job_id = job_id if job_id[-1] == 'm' else job_id + '-m'
		print job_id
		print subprocess.check_output('python ' + os.path.dirname(__file__) + '/log_job.py ' + job_id, shell=True)
else:
	print 'No jobs found'
