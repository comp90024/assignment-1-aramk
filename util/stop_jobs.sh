#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Stops and deletes current jobs for a user.

# Read args
if [ $# -lt 1 ]; then
	echo '$ ./stop_jobs.sh <user>'
	exit 1
fi
if [ ! -z "$1" ]; then
	USER=$1
fi

echo "Stopping Jobs"

# Stop and delete current jobs
qselect -u $USER | xargs -L1 qdel
