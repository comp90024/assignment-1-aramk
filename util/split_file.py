#!/usr/bin/env python

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

import sys, os.path, getopt

# Splits a file into equal parts without splitting a word into pieces and stores them under the given directory.

optlist, args = getopt.getopt(sys.argv[1:], 'l:o:')

if len(args) < 2:
	# -l : if == 1, lines are split rather than characters.
	# -o : the output directory
	print '$ python ./split_file.py [-l <split on lines>] [-o <output dir>] <file> <pieces>'
	exit()
else:
	source_file = args[0]
	piece_count = int(args[1])

splitlines = False
cleanlines = True
output_dir = os.path.dirname(source_file)
for o, a in optlist:
    if o == '-l' and a == '1':
        splitlines = True
    elif o == '-o':
    	output_dir = a

f = open(source_file)
content = f.read()

# TODO refactor with counter.py into a separate util
def clean(string):
    '''
    Removes unecessary formatting from a string.
    '''
    return string.lower().strip().replace("'", ' ')

pieces = []
if splitlines:
	lines = content.splitlines()
	if cleanlines:
		# Remove duplicates, lowercase
		lines = list(set([clean(l) for l in lines]))
	piece_size = len(lines) / piece_count
	piece_size = piece_size if piece_size > 0 else 1
	i = 0
	while i < len(lines):
		lines_piece = lines[i : i + piece_size]
		pieces.append('\r\n'.join(lines_piece))
		i += piece_size
else:
	piece_size = len(content) / piece_count
	i = 0
	while i < len(content):
		pieces.append(content[i : i + piece_size])
		i += piece_size

# If we have exceeded the number of pieces, append the last 2. This can happen due to integer division.
while len(pieces) > piece_count:
	pieces[-2] = pieces[-2] + pieces[-1]
	pieces.pop()

output_files = []
for i in range(len(pieces)):
	# filename, ext = os.path.splitext(source_file)
	# piece_filename = '%s_%d%s' % (filename, i + 1, ext)
	piece_filename = output_dir + ('/%s_%d' % (os.path.basename(source_file), i + 1))
	f = open(piece_filename, 'w')
	f.write(pieces[i])
	f.close()
	output_files.append(piece_filename)

print ' '.join(output_files)
