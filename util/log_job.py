#!/usr/bin/env python

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Logs information about a job.
# This needs Python 2.7+

import sys, re, os, subprocess

if len(sys.argv) < 2:
	print '$ python ./log_job.py <job id>'
	exit()
else:
	job_id = sys.argv[1]

is_array = re.search('\[', job_id) is not None
cmd = 'qstat -f ' + job_id + (' -t' if is_array else '')

try:
	log = subprocess.check_output(cmd, shell=True)
except Exception:
	exit()

def get_field(name, string):
	match = re.search(r'^.*' + name + r'.*$', string, flags=re.I | re.M)
	return match.group().strip() if match else None

jobs = re.findall(r'Job.*?\r?\n\r?\n', log, flags=re.I | re.M | re.S)
for job in jobs:
	for field in ['Job Id', 'Job_Name', 'job_state', 'ctime', 'etime', 'qtime', 'start_time', 'comp_time', 'cput', 'walltime']:
		value = get_field(field, job)
		print value if value else None
	print
