#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Clears all error and output files from the current directory.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

find . -regex ".*\.[oe][0-9]*" -type f -delete
# find . -regex "./words-.*" -type d -delete
find . -regex "./words-.*" -type d | xargs rm -rf
