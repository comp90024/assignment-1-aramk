#!/usr/bin/env python

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

# Runs the Counter on a set of source files and target file.
#       $ python ./run.py [-v] <source1> <source2> <source3> ... <target>
#       -v : Enables verbose mode.
# If more than one source file is provided, the results are merged.

from counter import *
import os.path, sys, getopt, time

# Start timer
start_time = time.time()

counter = WordCounter()

# Paths
data_dir = os.path.abspath(os.path.dirname(__file__) + '/../data') + '/'
target_path = 'ShakespeareComplete.txt'
source_paths = ['JttCoftE.txt', 'AToTCities.txt', 'WaP.txt']

# Read in command line options
optlist, args = getopt.getopt(sys.argv[1:], 'v')
verbose = False
for o, a in optlist:
    if o == '-v':
        verbose = True

if len(args) == 1:
    source_paths = args[0]
elif len(args) >= 2:
    source_paths = args[0:-1]
    target_path = args[-1]
else:
    target_path = data_dir + target_path
    source_paths = [data_dir + i for i in source_paths]

def print_maybe(s, b=True):
    if b:
        print s

print_maybe('Source Files: ' + ', '.join(source_paths), verbose)
print_maybe('Target File: ' + target_path, verbose)

times = []
counts = []
results = {}
for source_path in source_paths:
    process_time = time.time()

    source_file = open(source_path, 'r')
    target_file = open(target_path, 'r')
    source_str = source_file.read()
    target_str = target_file.read()

    print_maybe('Processing: %s' % source_path, verbose)
    print_maybe('', verbose)
    count = 0
    # Since Counter uses generators, it's possible to limit how many resuls we get back.
    for word, freq in counter.freq(source=source_str, target=target_str, source_limit=None, target_limit=None):
        results[word] = results[word] + freq if word in results else freq
        count += 1
    times.append(time.time() - process_time)
    counts.append(count)

for word, freq in results.iteritems():
    print '%s #%d' % (word, freq)

if verbose:
    def padded(padding, *args):
        return ('%' + str(padding) + 's: %.2f seconds') % args

    left_padding = max([len(i) for i in source_paths])
    for i in range(len(times)):
        print padded(left_padding, source_paths[i], times[i]) + (' (%d words)' % counts[i])
    print padded(left_padding, 'Total Time', time.time() - start_time)
    print ('%' + str(left_padding) + 's: %d') % ('Unique Words', len(results))
