# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

import re

class WordCounter:
    '''
    A class dedicated to counting words in text.
    '''
    
    def __init__(self):
        pass

    def freq(self, source, target, source_limit=None, target_limit=None):
        '''
        Finds the frequency of the words from source in target.
        Args:
            source: A string containing words.
            target: A string containing words.
            source_limit: The number of words to use from the source.
            target_limit: The number of words to use from the target.
        Returns:
            A generator which returns a tuple: (word:string, freq:int)
        '''
        source_words = self.word_map(source, limit=source_limit)
        target_words = self.word_map(target, limit=target_limit)
        for word in self.intersection_keys(source_words, target_words):
            yield (word, target_words[word])

    def word_map(self, string, limit=None):
        '''
        Finds all occurrences of words in the given string and returns
        a map of their frequencies.
        '''
        word_map = {}
        for match in self.re_words(string):
            word = self.clean(match.group())
            if word in word_map:
                word_map[word] = word_map[word] + 1;
            else:
                word_map[word] = 1;
                if limit > 1:
                    limit -= 1
                elif limit is not None:
                    break
        return word_map

    def intersection_keys(self, map_a, map_b):
        '''
        Returns a generator which forms a map with the intersecting
        keys of the two maps passed in.
        '''
        for i in map_a:
            if i in map_b:
                yield i

    def re_str_lines(self, string):
        '''
        Returns an iterator for each line in the string.
        '''
        return re.finditer(r'^.*$', string, flags=re.M | re.S)

    def re_words(self, string):
        '''
        Returns an iterator for each word in the string. Words can
        only contain letters.
        '''
        return re.finditer(r'\b[a-z]+\b', string, flags=re.I)

    def clean(self, string):
        '''
        Removes unecessary formatting from a string.
        '''
        return string.lower().strip().replace("'", ' ')
