#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

#PBS -N Assignment_1
#PBS -A pMelb0164
#PBS -q parallel
#PBS -l nodes=2:ppn=1
#PBS -l walltime=01:00:00
#PBS -l pmem=2000mb

module load python/2.7.3-gcc
python ~/assignment-1-aramk/src/run.py
