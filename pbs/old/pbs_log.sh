#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

#PBS -A pMelb0164
#PBS -q fast
#PBS -l walltime=01:00:00
#PBS -l pmem=2000mb

for JOB_ID in $JOB_IDS
do
	if [[ $JOB_ID == *"[]"* ]]
	then
		echo "Logging Array Job $JOB_ID"
		qstat -f $JOB_ID -t
	else
		echo "Logging Job $JOB_ID"
		qstat -f $JOB_ID
	fi
done
