#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

#PBS -A pMelb0164
#PBS -q parallel
#PBS -l walltime=01:00:00
#PBS -l pmem=2000mb

cd $PBS_O_WORKDIR

echo "Executing MERGE JOB # $PBS_JOBID, DIR: $(pwd), $(date "+%Y-%m-%d %H:%M:%S")"
# Removes duplicate entries and sorts ascending.
find $TASK_ID -type f | egrep ".*_output" | xargs cat | sort -u > $SOURCE_FILE_PREFIX"_merged"

echo "Completed Parallel Task # $TASK_ID $(date "+%Y-%m-%d %H:%M:%S")"
