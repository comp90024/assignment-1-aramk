#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

#PBS -A pMelb0164
#PBS -q parallel
#PBS -l walltime=01:00:00
#PBS -l pmem=2000mb

cd $PBS_O_WORKDIR
echo "Executing JOB # $PBS_JOBID, PBS_ARRAYID: $PBS_ARRAYID, DIR: $(pwd), $(date "+%Y-%m-%d %H:%M:%S")"

module load python/2.7.3-gcc
SOURCE_FILE_PIECE=$SOURCE_FILE_PREFIX"_"$PBS_ARRAYID
RUN_ARGS="$SOURCE_FILE_PIECE $TARGET_FILE"
python ./src/run.py $RUN_ARGS > $SOURCE_FILE_PIECE"_output"
