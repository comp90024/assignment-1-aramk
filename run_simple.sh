#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

# Runs the application in memory on the current machine.

module load python/2.7.3-gcc # This has no effect outside the Edward HPC
python ./src/run.py -v
