#!/bin/bash

# COMP90024 Cluster and Cloud Computing Assignment 1, Semester 2, 2013.  
# By Aram Kocharyan, 359867, aramk.

DATA_DIR="./data"
RUN_SCRIPT="./run_parallel.sh"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

if [ $# -lt 1 ]; then
	echo '$ ./run_parallel_all.sh <user>'
	exit 1
fi
USER=$1

SOURCE_FILES="AToTCities.txt JttCoftE.txt WaP.txt"
TARGET_FILE="ShakespeareComplete.txt"

echo "Stopping Jobs"
./util/stop_jobs.sh $USER > /dev/null 2>&1
./clean.sh

for SOURCE_FILE in $SOURCE_FILES
do
	for INDEX in {1..3}
	do
		NODE=$((2**INDEX))
		RUN_ARGS="$USER $NODE $DATA_DIR/$SOURCE_FILE $DATA_DIR/$TARGET_FILE"
		echo "> Starting Job: $RUN_SCRIPT $RUN_ARGS"
		$RUN_SCRIPT $RUN_ARGS
	done
done
